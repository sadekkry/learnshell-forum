"""learnshell URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r"^$", views.home, name="home")
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r"^$", Home.as_view(), name="home")
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r"^blog/", include("blog.urls"))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path

from forum.views import UserView, HomeView, ThreadView, ThreadCreateView, ThreadUpdateView, ThreadDeleteView, \
    CommentCreateView, CommentDeleteView, CommentUpdateView, CommentLikeView

urlpatterns = [
    path("admin/", admin.site.urls),

    path("login/", auth_views.login, {"template_name": "login.html"}, name="login"),
    path("logout/", auth_views.logout, name="logout"),

    path("user/<int:pk>", UserView.as_view(), name="user"),

    path("thread/<int:pk>", ThreadView.as_view(), name="thread"),
    path("thread/new", ThreadCreateView.as_view(), name="thread-new"),
    path("thread/update/<int:pk>", ThreadUpdateView.as_view(), name="thread-update"),
    path("thread/delele/<int:pk>", ThreadDeleteView.as_view(), name="thread-delete"),

    path("comment/new/<int:pk>", CommentCreateView.as_view(), name="comment-create"),
    path("comment/update/<int:pk>", CommentUpdateView.as_view(), name="comment-update"),
    path("comment/delele/<int:pk>", CommentDeleteView.as_view(), name="comment-delete"),
    path("comment/like/<int:pk>/<int:id>", CommentLikeView.as_view(), name="comment-like"),

    path("", HomeView.as_view(), name="home"),
    # url(r"^api-auth/", include("rest_framework.urls"))
]
