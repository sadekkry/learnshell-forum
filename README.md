# Learnshell Forum

This is basic project derived from
[django-project-template](https://gitlab.fit.cvut.cz/learnshell-new/django-project-template) which should demonstrate
aquired knowledge of Django framework and Python language itself.

## Website layout

**title**  
*user ribbon*  
*navigation*  
**current page title**  
*main content*  

## Website map

Site is divided into three major areas:
 - Home
 - User
 - Thread

Each of them offers different context. Following sections cover their functionality. Functionality is ordered from least
privileged to most. Logged user can do everything everyone can etc.

### Home

**Everyone** Default page. Displays list of all threads.  
**Logged user** Can start new thread from there.

### User

**Everyone** Lists every thread user created or commented on.  
**Logged user** -

### Thread

**Everyone** Shows detail of thread and comments to that thread.  
**Logged user** Can comment on that thread and edit/delete *their own* comments. Can (un)like every comment.  
**Thread author** Can edit or delete thread.

## Problems #wontfix

 - There is no visible indication if user already liked specific comment - link works as toggle.
 - Although user can (through URL) open edit box with other users threads/comments, it cannot be saved.
 - Possibly more
