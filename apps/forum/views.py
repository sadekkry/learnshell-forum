from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView, ListView, View
from rest_framework.exceptions import PermissionDenied

from .forms import CommentForm
from .models import Comment, Thread


# "default" view showing list of @Thread
class HomeView(ListView):
    model = Thread
    template_name = "home.html"
    paginate_by = 25
    ordering = ["-id"]


# displays all @Thread created/commented on by specific @User
class UserView(DetailView):
    model = User
    template_name = "user.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # get list of @Thread ids @User commented on
        thread_ids = set(Comment.objects.filter(author=self.kwargs["pk"]).values_list(flat=True))

        context["thread_user_list"] = Thread.objects.filter(author=self.kwargs["pk"]).order_by("id")
        context["thread_comment_list"] = Thread.objects.filter(author__in=thread_ids).order_by("id")
        context["user"] = self.request.user
        return context


# @Thread-oriented views:


class ThreadView(DetailView):
    model = Thread
    template_name = "thread.html"
    form_class = CommentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["comment_list"] = Comment.objects.filter(thread=context["thread"]).annotate(
            count=Count("like")).order_by("id")
        context["comment_form"] = CommentForm
        return context


class ThreadCreateView(LoginRequiredMixin, CreateView):
    model = Thread
    template_name = "thread_form.html"
    fields = ["title", "text"]

    def get_success_url(self):
        return reverse_lazy("thread", kwargs={"pk": self.object.id})

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(ThreadCreateView, self).form_valid(form)


class ThreadUpdateView(LoginRequiredMixin, UpdateView):
    model = Thread
    template_name = "thread_form.html"
    fields = ["text"]

    def form_valid(self, form):
        if self.request.user != self.object.author:
            raise PermissionDenied("You can't edit others ppl threads!")
        return super(ThreadUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy("thread", kwargs={"pk": self.object.id})


class ThreadDeleteView(LoginRequiredMixin, DeleteView):
    model = Thread
    template_name = "thread_delete.html"

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user != self.object.author:
            raise PermissionDenied("You can't delete others ppl threads!")
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy("home")


# @Comment-oriented views:


class CommentCreateView(LoginRequiredMixin, CreateView):
    model = Comment
    fields = ["text"]

    # disable GET request
    def get(self, request, *args, **kwargs):
        return redirect("thread", pk=self.kwargs["pk"])

    def get_success_url(self):
        return reverse_lazy("thread", kwargs={"pk": self.kwargs["pk"]})

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.thread = Thread.objects.get(id=self.kwargs["pk"])
        return super(CommentCreateView, self).form_valid(form)


class CommentUpdateView(UpdateView):
    model = Comment
    template_name = "comment_form.html"
    fields = ["text"]

    def form_valid(self, form):
        if self.request.user != self.object.author:
            raise PermissionDenied("You can't edit others ppl comments!")
        return super(CommentUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy("thread", kwargs={"pk": self.object.thread.id})


class CommentDeleteView(DeleteView):
    model = Comment
    template_name = "comment_delete.html"

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user != self.object.author:
            raise PermissionDenied("You can't delete others ppl comments!")
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy("thread", kwargs={"pk": self.object.thread.id})


class CommentLikeView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        obj = Comment.objects.get(id=self.kwargs["id"])
        if obj.like.filter(id=self.request.user.id).exists():
            obj.like.remove(self.request.user)
        else:
            obj.like.add(self.request.user)
        obj.save()
        return redirect("thread", pk=self.kwargs["pk"])
