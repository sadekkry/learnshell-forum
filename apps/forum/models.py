from django.contrib.auth.models import User
from django.db.models import Model, ForeignKey, DateField, CharField, TextField, ManyToManyField, CASCADE


class Thread(Model):
    author = ForeignKey(User, on_delete=CASCADE)
    date = DateField(auto_now=True)
    title = CharField(max_length=255, unique=True)
    text = TextField()

    def __str__(self):
        return self.title


class Comment(Model):
    thread = ForeignKey(Thread, on_delete=CASCADE)
    author = ForeignKey(User, on_delete=CASCADE)
    date = DateField(auto_now=True)
    text = TextField()
    like = ManyToManyField(User, related_name="userlike", blank=True)

    def __str__(self):
        return str(self.thread) + " / " + str(self.author)